<?php
include "header.php";
?>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <a href="#masakantambah" class="btn bg-pink waves-effect" data-toggle="modal">Tambah Masakan</a>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                      <tr>
                        <th>No</th>
                        <th>id Order</th>
                        <th>No Meja</th>
                        <th>Tanggal</th>
                        <th>Detail Order</th>
                        <th>Transaksi</th>
                      </tr>
                    </thead>
                    <tbody>
<?php
include "../koneksi.php";
$no=1;
$data = "SELECT * from detail_order INNER JOIN pesan_meja ON detail_order.id_order = pesan_meja.id_order INNER JOIN meja ON pesan_meja.no_meja = meja.no_meja INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan where status_order='N' GROUP BY detail_order.id_order";
$bacadata = mysqli_query($conn, $data);
while($r = mysqli_fetch_array($bacadata))
{
    $jumlah_harga = $r['harga'] * $r['jumlah'];
?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $r['id_order']; ?></td>
                        <td><?php echo $r['no_meja']; ?></td>
                        <td><?php echo $r['tanggal']; ?></td>
                        <td><a href="detail_transaksi.php?table=detail_order&id_order=<?php echo $r['id_order'];?>" class="btn btn-success">Detail</a></td>
                        <td><a href="#" class="btn btn-danger" data-toggle="modal" data-target="#bayar<?php echo $r['id_order'];?>">Bayar</a></td>
                      </tr>
                      <!-- #END# Basic Examples -->
 <div class="modal fade" id="bayar<?php echo $r['id_order'];?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Transaksi</h4>
            </div>
<div class="modal-body">
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="body">
                <form id="form_validation" action="proses_transaksi.php" method="POST">
                    <div class="form-group form-float">
                        <input type="hidden" class="form-control" value="<?php echo $r['no_meja'];?>" name="no_meja" >
                        <input type="hidden" class="form-control" value="N" name="status_meja" >
                        <input type="hidden" class="form-control" value="Y" name="status_order" >

      <?php 
        include "../koneksi.php";
          $username=$_SESSION['username'];
          $query_mysqli = mysqli_query($conn, "SELECT * FROM user INNER JOIN level ON user.id_level = level.id_level where username='$_SESSION[username]'")or die(mysqli_error());
              while($data = mysqli_fetch_array($query_mysqli)){
      ?>
                        <input type="hidden" class="form-control" value="<?php echo $data['id_user']; ?>" name="id_user" >
        <?php } ?>
                        <input type="hidden" class="form-control" value="<?php echo $r['id_order']; ?>" name="id_order" >
                        <div class="form-line">
                            <input type="text" class="form-control" value="<?php echo $jumlah_harga; ?>" name="total_bayar" readonly>
                            <label class="form-label">Total Yg Dibayarkan</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="yg_dibayarkan" required>
                            <label class="form-label">Jumlah Uang</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- #END# Basic Validation -->
                        </div>
                    </div>
                </div>
            </div>
<?php } ?>
                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
<?php
include "footer.php";
?>