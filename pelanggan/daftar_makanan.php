<?php  
include "header.php";
?>
	<!-- Content page -->
	<section class="bgwhite p-t-55 p-b-65">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-12 col-lg-12 p-b-50">
					<!-- Product -->
					<div class="row">
<?php
include "../koneksi.php";
$data = "SELECT * from masakan where jenis='Makanan' and status_masakan='Y'";
$bacadata = mysqli_query($conn, $data);
while($r = mysqli_fetch_array($bacadata))
{
?>
						<div class="col-sm-12 col-md-6 col-lg-3 p-b-50">
							<!-- Block2 -->
							<div class="block2">
								<div class="block2-img wrap-pic-w of-hidden pos-relative block2">
									<img src="<?php echo "../admin/images/".$r['gambar']; ?>" height="300" alt="IMG-PRODUCT">

									<div class="block2-overlay trans-0-4">
										<div class="block2-btn-addcart w-size1 trans-0-4">
											<button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
												<a href="proses_cart.php?act=add&amp;id_masakan=<?php echo $r['id_masakan']; ?> &amp;ref=daftar_menu.php">Add to Cart</a>
											</button>
										</div>
									</div>
								</div>

								<div class="block2-txt p-t-20">
									<h4><a href="#" type="hidden" class="block2-name dis-block s-text3 p-b-5">
									<?php echo $r['nama_masakan']; ?>
								</a></h4>
								<p><?php echo $r['desk_masakan']; ?></p>
								<span class="block2-price m-text6 p-r-5">
									Rp.<?php echo $r['harga']; ?>
								</span>
								</div>
							</div>
						</div>
<?php
}
?>
</div>
				</div>
			</div>
		</div>
	</section>
	<?php  
include "footer.php";
?>