<?php  
include "header.php";
?>

	<!-- Slide1 -->
	<section class="slide1">
		<div class="wrap-slick1">
			<div class="slick1">
				<div class="item-slick1 item1-slick1" style="background-image: url(../assets/images/utama.jpg);">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
						<h2 class="caption1-slide1 xl-text2 t-center bo14 p-b-6 animated visible-false m-b-22" data-appear="fadeInUp">
							Leather Bags
						</h2>

						<span class="caption2-slide1 m-text1 t-center animated visible-false m-b-33" data-appear="fadeInDown">
							New Collection 2018
						</span>

						<div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="zoomIn">
							<!-- Button -->
							<a href="#" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
								Shop Now
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Title Page -->
	<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m">
		<h2 class="l-text2 t-center">
			Daftar Menu
		</h2>
		<p class="m-text13 t-center">
			Nikmati Dan Rasakan Sensasi Bapernya
		</p>
	</section>
	<!-- Banner -->
	<div class="banner bgwhite p-t-40 p-b-40">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="../assets/images/makanan.jpg" height="350" alt="IMG-BENNER">
						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="daftar_makanan.php" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Makanan
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="../assets/images/minuman.jpg" height="350" alt="IMG-BENNER">

						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="daftar_minuman.php" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Minuman
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto">
					<!-- block1 -->
					<div class="block1 hov-img-zoom pos-relative m-b-30">
						<img src="../assets/images/paketann.jpg" height="350" alt="IMG-BENNER">
						<div class="block1-wrapbtn w-size2">
							<!-- Button -->
							<a href="daftar_paketan.php" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4">
								Paketan
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- New Product -->
	<section class="newproduct bgwhite p-t-45 p-b-105">
		<div class="container">
			<div class="sec-title p-b-60">
				<h3 class="m-text5 t-center">
					Menu Populer
				</h3>
			</div>

			<!-- Slide2 -->
			<div class="wrap-slick2">
				<div class="slick2">
<?php
include "../koneksi.php";
$data = "SELECT * from masakan where status_masakan='Y'";
$bacadata = mysqli_query($conn, $data);
while($r = mysqli_fetch_array($bacadata))
{
?>
					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<div class="block2-img wrap-pic-w of-hidden pos-relative block2">
								<img src="<?php echo "../admin/images/".$r['gambar']; ?>" height="300" alt="IMG-PRODUCT">

								<div class="block2-overlay trans-0-4">
									<a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
										<i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
										<i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
									</a>

									<div class="block2-btn-addcart w-size1 trans-0-4">
										<!-- Button -->
										<button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
											Add to Cart
										</button>
									</div>
								</div>
							</div>

							<div class="block2-txt p-t-20">
								<h4><a href="#" type="hidden" class="block2-name dis-block s-text3 p-b-5">
									<?php echo $r['nama_masakan']; ?>
								</a></h4>
								<p><?php echo $r['desk_masakan']; ?></p>
								<span class="block2-price m-text6 p-r-5">
									Rp.<?php echo $r['harga']; ?>
								</span>
							</div>
						</div>
					</div>
<?php } ?>
				</div>
			</div>

		</div>
	</section>
<?php  
include "footer.php";
?>