<?php
include 'header.php';
?>
        <div class="card">
            <div class="body">
                <form id="forgot_password" method="POST">
                    <div class="msg">
                        Masukan alamat email yg digunakan saat mendaftar dan kami akan mengirim token untuk merubah kata sandi..
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Email" required autofocus>
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">SEND</button>

                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="index.php">Sign In!</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
include 'footer.php';
?>