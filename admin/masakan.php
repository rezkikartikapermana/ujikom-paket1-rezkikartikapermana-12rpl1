<?php
include "header.php";
?>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <a href="#masakantambah" class="btn bg-pink waves-effect" data-toggle="modal">Tambah Masakan</a>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                     <thead>
                      <tr>
                        <th>No</th>
                        <th>Id Masakan</th>
                        <th>Nama</th>
                        <th>Keterangan</th>
                        <th>Jenis</th>
                        <th>Gambar</th>
                        <th>Harga</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
<?php
include "../koneksi.php";
$no=0;
$data = "SELECT * from masakan";
$bacadata = mysqli_query($conn, $data);
while($select_result = mysqli_fetch_array($bacadata))
{
$no++;
$id_masakan          = $select_result['id_masakan'];
$nama_masakan        = $select_result['nama_masakan'];
$keterangan          = $select_result['desk_masakan'];
$jenis          = $select_result['jenis'];
$gambar              = "images/".$select_result['gambar'];
$harga               = $select_result['harga'];
$status              = $select_result['status_masakan'];

echo"<tr>
<td>$no</td> 
<td>$id_masakan</td>
<td>$nama_masakan</td>
<td>$keterangan</td>
<td>$jenis</td>
<td><img src='$gambar' height='100'></td>
<td>$harga</td>
";
//ganti imagesup dengan nama folder dimana anda menempatkan image hasil upload
?>
<td>
  <a href="#" class="btn btn-warning btn-md" data-toggle="modal" data-target="#makananedit<?php echo $id_masakan;?>">Edit</a>
  <?php
                                            if($status == 'Y')
                                            {
                                              ?>
                                            <a href="status_masakan.php?table=masakan&id_masakan=<?php echo $id_masakan;?>&action=not-verifed" class="btn btn-info btn-md">
                                            Tersedia
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                              
                                            <a href="status_masakan.php?table=masakan&id_masakan=<?php echo $id_masakan;?>&action=verifed" class="btn btn-danger btn-md">
                                            Tidak Tersedia
                                            </a>
                                            <?php
                                            }
                                            ?>
</td>
</tr>
 <div class="modal fade" id="makananedit<?php echo $id_masakan;?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Edit Masakan</h4>
            </div>
<?php
$query_edit = mysqli_query($conn,"SELECT * FROM masakan WHERE id_masakan='$id_masakan'");
$r = mysqli_fetch_array($query_edit);
?>
<div class="modal-body">
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="body">
                <form id="form_validation" action="proses_edit_masakan.php?id_masakan=<?php echo $id_masakan;?>" enctype="multipart/form-data" class="form-horizontal form-material" method="POST">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" value="<?php echo $r['id_masakan'];?>" disabled required>
                            <label class="form-label">Id User</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="nama_masakan" value="<?php echo $r['nama_masakan'];?>" required>
                            <label class="form-label">Nama Masakan</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="harga" value="<?php echo $r['harga'];?>" placeholder="Masukan Harga" required>
                            <label class="form-label">Harga</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="file" name="gambar" value="<?php echo $r['gambar'];?>">
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea class="form-control" name="keterangan" rows="3" required><?php echo $r['keterangan'];?></textarea>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input name="group1" type="radio" class="with-gap" id="radio_3" checked />
                                <label for="radio_3">Tersedia</label>
                            <input name="group1" type="radio" id="radio_4" class="with-gap" />
                                <label for="radio_4">Habis</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                         <!-- #END# Basic Validation -->
                        </div>
                    </div>
                </div>
            </div>


                    </tbody>
                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
 <div class="modal fade" id="masakantambah" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Edit Masakan</h4>
            </div>
<div class="modal-body">
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="body">
                <form id="form_validation" action="proses.php?aksi=tambah" method="POST">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" name="nama_masakan" required>
                                                        <label class="form-label">Nama Masakan</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <label>Jenis :</label>
                                                            <select name="id_level" class="form-control">
                                                                <option>Pilih--</option>
                                                                <option value="Makanan">Makanan</option>
                                                                <option value="Minuman">Minuman</option>
                                                                <option value="Paketan">Paketan</option>
                                                            </select>
                                                    </div>
                                                </div></br>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" name="harga" required>
                                                        <label class="form-label">Harga Masakan</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                      <input type="file" name="gambar">
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <textarea class="form-control" name="keterangan" rows="3" required></textarea>
                                                        <label class="form-label">Keterangan</label>
                                                    </div>
                                                </div>
                                                <input type="hidden" class="form-control" value="N" id="exampleInputPassword1" name="status_masakan">
                                                <div class="modal-footer">
                            <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                                            </form>                                        </div>
                                    </div>
                                </div>
                         <!-- #END# Basic Validation -->
                        </div>
                    </div>
                </div>
            </div>
            
<?php
include "footer.php";
?>