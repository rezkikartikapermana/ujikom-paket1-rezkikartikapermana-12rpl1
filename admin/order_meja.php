<?php
include "header.php";
?>
 <!-- Basic Example -->
 
            <div class="row clearfix">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header bg-light-green">
                            <h2>
                                Registrasi Meja
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li>
                                    <a href="" data-toggle="modal" data-target="#registrasi">
                                        <i class="material-icons">add_shopping_cart</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <img src='images/kiteleee.png'>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
<?php
include "../koneksi.php";
$no=1;
$data = "SELECT * from meja where status_meja='N'";
$bacadata = mysqli_query($conn, $data);
while($r = mysqli_fetch_array($bacadata))
{
?>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header bg-cyan">
                            <h2>
                                No.0<?php echo $r['no_meja']; ?>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li>
                                    <a href="" data-toggle="modal" data-target="#order_meja<?php echo $r['no_meja'];?>">
                                        <i class="material-icons">add_shopping_cart</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <img src='<?php echo "images/".$r['gambar']; ?>'>
                        </div>
                    </div>
                </div>
                            <!-- Small Size -->
            <div class="modal fade" id="order_meja<?php echo $r['no_meja'];?>" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="smallModalLabel">Order Meja</h4>
                        </div>
<?php
$no_meja = $r['no_meja'];
$angka= mt_rand(11111,99999); 
$query_edit = mysqli_query($conn,"SELECT * FROM meja WHERE no_meja='$no_meja'");
$r = mysqli_fetch_array($query_edit);
?>
                        <div class="modal-body">
                            <form id="form_validation" action="proses_order_meja.php?no_meja=<?php echo $no_meja;?>" enctype="multipart/form-data" class="form-horizontal form-material" method="POST">
                    <div class="form-group form-float">
                        <div class="form-line">
                        <input type="text" class="form-control" name="no_meja" value="<?php echo $r['no_meja'];?>" readonly>
                            <label class="form-label">No Meja</label>
                        </div>
                    </div></br>
                    <input type="hidden" class="form-control" value="Y" name="status_meja">
                    <input type="hidden" class="form-control" value="1" name="id_user">
                    <input type="hidden" class="form-control" value="Dipesan" name="keterangan">
                    <input type="hidden" class="form-control" value="N" name="status_order">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" value="<?php echo $angka;?>" name="token" readonly>
                            <label class="form-label">Token</label>
                        </div>
                    </div></br>
                    <input type="hidden" class="form-control" value="NULL" name="nama_pelanggan">
                    <div class="modal-footer">
                            <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
<?php }  ?>
            </div>
            <!-- #END# Basic Example -->


            <!-- Regist Modal -->
            <div class="modal fade" id="registrasi" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Order Meja</h4>
                        </div>
                        <div class="modal-body">
                            <form id="form_validation" action="proses_order_meja.php?no_meja=<?php echo $no_meja;?>" enctype="multipart/form-data" class="form-horizontal form-material" method="POST">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <?php 
                           include "../koneksi.php";
                                $data = "SELECT * from meja where status_meja='N'";
                                $bacadata = mysqli_query($conn, $data);
                                while($r = mysqli_fetch_array($bacadata))
                                {
                                    $no_meja = $r['no_meja'];
                             ?>
                            <input name="no_meja" type="radio" class="with-gap" id="radio<?php echo $no_meja ?>" />
                                <label for="radio<?php echo $no_meja ?>"><?php echo $no_meja;?></label>
                                <?php } ?>
                        </div>
                    </div></br></br></br>
                    <div class="form-group form-float">
                        <div class="form-line">
                        <?php
                            $angka= mt_rand(11111,99999); 
                        ?>
                            <input type="text" class="form-control" name="token" value="<?php echo $angka;?>" disabled>
                            <label class="form-label">Token</label>
                        </div>
                    </div></br>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="nama_pelanggan" required>
                            <label class="form-label">Nama Pelanggan</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                            <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
<?php
include "footer.php";
?>