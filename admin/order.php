<?php
include "header.php";
?>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                   <thead>
                      <tr>
                        <th>No</th>
                        <th>id Order</th>
                        <th>Nama User</th>
                        <th>No Meja</th>
                        <th>Tanggal</th>
                        <th>Pemesanan</th>
                        <th>Detail Order</th>
                        <th>Verifikasi Order</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
include "../koneksi.php";
$no=0;
$data = "SELECT * from pesan_meja INNER JOIN user ON pesan_meja.id_user = user.id_user where status_order='N' ORDER BY tanggal DESC";
$bacadata = mysqli_query($conn, $data);
while($select_result = mysqli_fetch_array($bacadata))
{
$no++;
$id_order         = $select_result['id_order'];
$nama_user        = $select_result['nama_pelanggan'];
$no_meja          = $select_result['no_meja'];
$tanggal          = $select_result['tanggal'];
$status          = $select_result['keterangan'];

echo"<tr>
<td>$no</td> 
<td>$id_order</td>
<td>$nama_user</td>
<td>$no_meja</td>
<td>$tanggal</td>
";
//ganti imagesup dengan nama folder dimana anda menempatkan image hasil upload
?>
                        <td><a href="order_masakan.php?id_order=<?php echo $id_order;?>" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i></a></td>
                        <td><a href="detail_order.php?table=detail_order&id_order=<?php echo $id_order;?>" class="btn btn-warning">Detail</a></td>
                        <td><?php
                                            if($status == 'Y')
                                            {
                                              ?>
                                            <a href="#" class="btn btn-primary btn-md">
                                            Menunggu Pesanan
                                            </a>
                                            <?php
                                            }else{
                                              ?>
                                              
                                            <a href="#" class="btn btn-danger btn-md">
                                            Belum Memesan
                                            </a>
                                            <?php
                                            }
                                            ?></td>
                      </tr>
                      <?php }  ?>
                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
 <div class="modal fade" id="masakantambah" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Edit Masakan</h4>
            </div>
<div class="modal-body">
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="body">
                <form id="form_validation" action="proses.php?aksi=tambah" method="POST">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" name="nama_masakan" required>
                                                        <label class="form-label">Nama Masakan</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <label>Jenis :</label>
                                                            <select name="id_level" class="form-control">
                                                                <option>Pilih--</option>
                                                                <option value="Makanan">Makanan</option>
                                                                <option value="Minuman">Minuman</option>
                                                                <option value="Paketan">Paketan</option>
                                                            </select>
                                                    </div>
                                                </div></br>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" name="harga" required>
                                                        <label class="form-label">Harga Masakan</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                      <input type="file" name="gambar">
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <textarea class="form-control" name="keterangan" rows="3" required></textarea>
                                                        <label class="form-label">Keterangan</label>
                                                    </div>
                                                </div>
                                                <input type="hidden" class="form-control" value="N" id="exampleInputPassword1" name="status_masakan">
                                                <div class="modal-footer">
                            <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                                            </form>                                        </div>
                                    </div>
                                </div>
                         <!-- #END# Basic Validation -->
                        </div>
                    </div>
                </div>
            </div>
            
<?php
include "footer.php";
?>