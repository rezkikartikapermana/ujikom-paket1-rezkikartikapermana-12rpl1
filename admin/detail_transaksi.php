<?php
include "header.php";
?>

            <div class="row clearfix">
                <!-- Basic Examples -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion_3" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-warning">
                                            <div class="panel-heading" role="tab" id="headingOne_3">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion_3" href="#collapseOne_3" aria-expanded="true" aria-controls="collapseOne_3">
                                                        Detail Order
                                                    </a>
                                                </h4>
                                            </div>
                                            <?php
include "../koneksi.php";

$edit = mysqli_query($conn, "SELECT * FROM pesan_meja where id_order='$_GET[id_order]'");
$r=mysqli_fetch_array($edit);
?>
                                            <div id="collapseOne_3" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_3">
                                                <div class="panel-body">
                                                    <form id="form_validation" role="form">
                                                        <div class="form-group form-float">
                                                            <div class="form-line">
                                                            <label>No Meja</label>
                                                            <input type="text" class="form-control" name="no_meja" value="<?php echo $r['no_meja'];?>" disabled/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-float">
                                                            <div class="form-line">
                                                            <label>Nama Pelanggan</label>
                                                            <input type="text" class="form-control" value="<?php echo $r['nama_pelanggan'];?>" disabled/>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion_19" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-col-pink">
                                            <div class="panel-heading" role="tab" id="headingOne_19">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_19" aria-expanded="true" aria-controls="collapseOne_19">
                                                        <i class="material-icons">perm_contact_calendar</i> Detail Masakan
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_19" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_19">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Pesanan</th>
                        <th>Keterangan</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                  <?php
include "../koneksi.php";
$no=1;
$total=0;
$data = "SELECT * FROM detail_order INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan where id_order='$_GET[id_order]'";
$bacadata = mysqli_query($conn, $data);
while($r= mysqli_fetch_array($bacadata))
{
    $jumlah_harga    = $r['harga'] * $r['jumlah'];
    $total          += ($r['jumlah']*$r['harga']);
    $id_order      = $r['id_order'];
?>
                    <tbody>
                      <tr>
                        <td><?php echo $no++ ;?></td>
                        <td><?php echo $r['nama_masakan'];?></td>
                        <td><?php echo $r['keterangan_detail'];?></td>
                        <td><?php echo $r['jumlah'];?></td>
                        <td><?php echo $r['harga'];?></td>
                        <td><?php echo $jumlah_harga?></td>
                      </tr>
                    </tbody>
<?php 
                  }
                   ?>
                    <tfoot>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Total :</th>
                        <th><?php echo $total; ?></th>
                      </tr>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><a href="#" class="btn btn-danger" data-toggle="modal" data-target="#bayar<?php echo $id_order;?>">Bayar</a></th>
                      </tr>
                    </tfoot>
                      <!-- #END# Basic Examples -->
 <div class="modal fade" id="bayar<?php echo $id_order;?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Transaksi</h4>
            </div>
<div class="modal-body">
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="body">
                <form id="form_validation" action="proses_transaksi.php" method="POST">
                    <div class="form-group form-float">
                        <input type="hidden" class="form-control" value="N" name="status_meja" >
                        <input type="hidden" class="form-control" value="Y" name="status_order" >
      <?php 
        include "../koneksi.php";
          $username=$_SESSION['username'];
          $query_mysqli = mysqli_query($conn, "SELECT * FROM user INNER JOIN level ON user.id_level = level.id_level where username='$_SESSION[username]'")or die(mysqli_error());
              while($data = mysqli_fetch_array($query_mysqli)){
      ?>
                        <input type="hidden" class="form-control" value="<?php echo $data['id_user']; ?>" name="id_user" >
        <?php } ?>
                        <input type="hidden" class="form-control" value="<?php echo $id_order; ?>" name="id_order" >
                        <div class="form-line">
                            <input type="text" class="form-control" value="<?php echo $total; ?>" name="total_bayar" readonly>
                            <label class="form-label">Total Yg Dibayarkan</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="yg_dibayarkan" required>
                            <label class="form-label">Jumlah Uang</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- #END# Basic Validation -->
                                </table>
                            </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Multiple Items To Be Open -->

                        </div>
                    </div>
                </div>
            </div>
<?php
include "footer.php";
?>